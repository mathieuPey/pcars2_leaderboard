package com.pcars2.leaderboard.DTO

interface TrackDTO {
    var id: String
    var name: String
}