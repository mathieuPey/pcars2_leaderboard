package com.pcars2.leaderboard.DTO

data class Tier (
    var level: Int = 0,
    var minTime: String = "",
    var maxTime: String = "",
    var nbUser: Int = 0
)