package com.pcars2.leaderboard.DTO

data class ComboCardDTO (
    val trackName: String = "",
    val vehicleName: String = "",
    val bestOfAllTime: String,
    val personalBestTime: String,
    val nbLaptime: Int,
    val personalRanking: Int,
    val personalTier: String,
    val gapToNextTier: String,
    val tiers: List<Tier>
)

data class ComboCardSQLDTO (
    val chrono: Int,
    val date: String,
    val username: String,
    val vehicleName: String,
    val trackName: String
)