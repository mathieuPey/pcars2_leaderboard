package com.pcars2.leaderboard.DTO

interface UserDTO {
    var id: String
    var name: String
}