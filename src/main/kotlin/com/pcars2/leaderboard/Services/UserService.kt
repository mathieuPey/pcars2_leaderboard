package com.pcars2.leaderboard.Services

import com.pcars2.leaderboard.DAO.UserDao
import com.pcars2.leaderboard.DTO.UserDTO
import com.pcars2.leaderboard.Entities.User
import org.springframework.stereotype.Service

@Service
class UserService(
        private val userDao: UserDao
) {
    fun save(user: User) {
        userDao.save(user)
    }

    fun getAllUser(): List<User> {
        return userDao.findAll().toList()
    }

    fun getAllUserDTO(): List<UserDTO> {
        return userDao.findAllDTO().toList()
    }

    fun getById(id: Long?): List<User> {
        return userDao.findAll().filter { it -> it.id == id }
    }

    fun getByName(username: String?): List<User> {
        return userDao.findByName(username)
    }

    fun foundAllByName(partialName: String?): List<User> {
        return userDao.foundAllByName(partialName)
    }
}