package com.pcars2.leaderboard.Services

import com.pcars2.leaderboard.DAO.ComboCardDao
import com.pcars2.leaderboard.DAO.TrackDao
import com.pcars2.leaderboard.DAO.UserDao
import com.pcars2.leaderboard.DTO.ComboCardDTO
import com.pcars2.leaderboard.DTO.ComboCardSQLDTO
import com.pcars2.leaderboard.DTO.Tier
import com.pcars2.leaderboard.Utils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.ComponentScan
import org.springframework.stereotype.Service

@Service
class ComboCardService  (
        @Autowired
        private val comboCardDao: ComboCardDao,
        private val trackDao: TrackDao,
        private val userDao: UserDao
) {
    fun foundByUser(idUser: Long?): List<ComboCardDTO> {
        var result: MutableList<ComboCardDTO> = mutableListOf()
        var userName = userDao.getNameById(idUser)
        var trackList = trackDao.foundByUser(idUser)

        trackList.forEach {
            var comboSQLDTO = comboCardDao.foundByComboTrackUser(it.id, idUser).map { transformToComboSQLDTO(it) }
            var combo = transformToComboCardDTO(comboSQLDTO, userName)

            combo.forEach { result.add(it) }
        }

        return result
    }

    fun foundByComboTrackUser(idTrack: Long?, idUser: Long?): List<ComboCardDTO> {
        var result: List<ComboCardDTO>
        var userName = userDao.getNameById(idUser)

        var comboSQLDTO = comboCardDao.foundByComboTrackUser(idTrack, idUser).map { transformToComboSQLDTO(it) }
        result = transformToComboCardDTO(comboSQLDTO, userName)

        return result
    }



    private fun transformToComboSQLDTO(comboSQL: ComboCardDao.ComboCardSQL): ComboCardSQLDTO {
        return ComboCardSQLDTO(comboSQL.chrono, comboSQL.date, comboSQL.username, comboSQL.vehicleName, comboSQL.trackName)
    }

    private fun transformToComboCardDTO(combosSQL: List<ComboCardSQLDTO>, username: String): List<ComboCardDTO> {
        var listDTO: MutableList<ComboCardDTO> = mutableListOf()

        // On les regroupe par vehicule
        var groups = combosSQL.groupBy { it.vehicleName }

        // Pour chaque vehicule, on construit un DTO, qu'on ajoute à la liste
        groups.forEach{
            val bestTime = it.value.minBy { it.chrono }
            val pr = it.value.filter { it.username == username }.first()
            val nbLaptime = it.value.count()
            val personalRanking = it.value.sortedBy { it.chrono }.indexOf(it.value.find { it.username == username })
            val tiers = Utils.composeTiers(combosSQL)
            val myTier = Utils.computeMyTier(tiers, pr.chrono)
            val gapToNextTier = Utils.computeGapToNextTier(tiers, pr.chrono).toString()

            var comboCard: ComboCardDTO = ComboCardDTO(it.value.first().trackName
                    , it.value.first().vehicleName
                    , Utils.convertMillisToFormattedString(bestTime!!.chrono)
                    , Utils.convertMillisToFormattedString(pr.chrono)
                    , nbLaptime
                    , personalRanking
                    , myTier
                    , Utils.convertMillisToFormattedString(gapToNextTier.toInt())
                    , tiers
            )

            listDTO.add(comboCard)
        }

        return listDTO
    }
}