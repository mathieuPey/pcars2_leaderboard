package com.pcars2.leaderboard.Services

import com.pcars2.leaderboard.Entities.Laptime
import com.pcars2.leaderboard.Entities.Track
import com.pcars2.leaderboard.Entities.User
import com.pcars2.leaderboard.Entities.Vehicles
import com.pcars2.leaderboard.Utils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.springframework.stereotype.Service
import java.lang.Exception
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Service
class ScrapService (
        private val userService: UserService
) {
    private var currentTrack: Track = Track()
    private var currentVehicle: Vehicles = Vehicles()

    fun scrapVehicles(): List<Vehicles> {
        var doc = this.getDocument()

        return getCarList(doc)
    }

    fun scrapTracks(): List<Track> {
        var doc = this.getDocument()

        return getTrackList(doc)
    }

    // Return all laptimes for a combination of a track and an User
    fun scrapLaptimes(track: Track, vehicle: Vehicles): List<Laptime> {
        var doc = this.getDocument("index.php/leaderboard?track=${track.id}&vehicle=${vehicle.id}")
        this.currentTrack = track
        this.currentVehicle = vehicle

        return getLaptimes(doc)
    }


    private fun getDocument(urlAppend: String = "", urlBaseToClean: Boolean = false): Document {
        var doc: Document

        if (urlBaseToClean) {
           doc = Jsoup.connect(urlAppend).get()
        } else {
           doc = Jsoup.connect("http://cars2-stats-steam.wmdportal.com/${urlAppend}").get()
        }

        return doc
    }

    private fun getCarList(doc: Document): List<Vehicles> {
        val carsList = doc.select("#select_leaderboard_vehicle > option")
                .map { option -> convertToVehicle(option.attr("value").toLong(), option.text()) }
                .filter { it.id != 0L }

        return carsList
    }

    private fun convertToVehicle(id: Long, name: String): Vehicles {
        return Vehicles(id, name)
    }

    private fun getTrackList(doc: Document): List<Track> {
        val trackList = doc.select("#select_leaderboard_track > option")
                .map{ option -> Track(option.attr("value").toLong(), option.text()) }
                .filter { it.id != 0L }

        return trackList
    }

    private fun getLaptimes(doc: Document): List<Laptime> {
        var listLaptime: List<Laptime> = listOf()

        // Is this multi-page, and if so, how many
        val dropdownListPage = doc.select("#pager_top_select_page > option")
        val isMultipage = dropdownListPage.size != 0

        if (isMultipage) {
            val nbPage = dropdownListPage.last().text().toInt()
            val baseUri = doc.baseUri()

            // iterate on nb page
            for(pageNumber in 1..nbPage) {
                var newUrl = baseUri.plus("&page=${pageNumber}")
                listLaptime += getLaptimesFromCurrent(getDocument(newUrl, true))
            }

        } else {
            listLaptime = getLaptimesFromCurrent(doc)
        }

        return listLaptime
    }

    private fun getLaptimesFromCurrent(doc: Document): List<Laptime> {
        val laptimeList = doc.select("#leaderboard > tbody > tr")
                .filter { it.select(".user").size > 0 }
                .map{ element ->
                    convertToLaptime(element) }

        return laptimeList
    }

    private fun convertToLaptime(el: Element): Laptime {
            // Extract User
            val userElement = el.select(".user")
            val userId = userElement.attr("id").split('_')[1].toLong()
            val user = if (userService.getById(userId).isNotEmpty()) userService.getById(userId).first()
            else User(userId, userElement.text())
            userService.save(user)

            // Extract chrono
            val chrono = el.select(".time > span.time").text()
            val chronoInSeconds = Utils.convertStringToSeconds(chrono)

            // Extract date chrono
            val dateChrono = el.select(".timestamp").text()
            var ldt = LocalDateTime.MIN
            if (dateChrono.isNotBlank()) {
                ldt = LocalDateTime.parse(dateChrono, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"))
            }

            return Laptime(currentTrack.id, user.id,  currentVehicle.id, chronoInSeconds, ldt)
    }
}