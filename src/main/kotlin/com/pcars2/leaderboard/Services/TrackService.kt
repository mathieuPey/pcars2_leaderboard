package com.pcars2.leaderboard.Services

import com.pcars2.leaderboard.DAO.TrackDao
import com.pcars2.leaderboard.DTO.TrackDTO
import com.pcars2.leaderboard.Entities.Track
import org.springframework.stereotype.Service
import java.util.*

@Service
class TrackService(
        private val trackDao: TrackDao
) {
    fun save(track: Track) {
        trackDao.save(track)
    }

    fun getAllTracks(): List<Track> {
        return trackDao.findAll().toList()
    }

    fun getById(id: Long?): List<Track> {
        return trackDao.findAll().filter { it.id == id }
    }

    fun getByName(trackname: String?): List<Track> {
       return trackDao.findAll().toList().filter { it.name.contains(trackname.toString()) }
    }

    fun getAllTracksByUserId(userId: Long?): List<Track> {
        return trackDao.foundByUser(userId).toList()
    }



    fun getAllTracksDTO(): List<TrackDTO> {
        return trackDao.findAllDTO().toList()
    }

    fun getAllTracksDTOByUserId(userId: Long?): List<TrackDTO> {
        return trackDao.foundDTOByUser(userId).toList()
    }
}