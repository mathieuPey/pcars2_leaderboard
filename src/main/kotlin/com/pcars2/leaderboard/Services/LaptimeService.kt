package com.pcars2.leaderboard.Services

import com.pcars2.leaderboard.DAO.LaptimeDao
import com.pcars2.leaderboard.Entities.Laptime
import org.springframework.stereotype.Service

@Service
class LaptimeService (
        private val laptimeDao: LaptimeDao
) {
    fun save(laptime: Laptime) {
        laptimeDao.save(laptime)
    }

    fun getAllLaptimes(): List<Laptime> {
        return laptimeDao.findAll().toList()
    }

    fun getAllByUserId(id: Long): List<Laptime> {
        return laptimeDao.findByIdUser(id).toList()
    }

    fun getAllByTrackAndVehicle(idTrack: Long, idVehicle: Long): List<Laptime> {
        return laptimeDao.findByIdTrackAndIdVehicles(idTrack, idVehicle)
    }

//    fun getAllCustom(idUser: Int = -1, nameUser: String = ""): List<Laptime> {
//        return laptimeDao.findCustom(idUser)
//    }

    fun getCountForCombo(idTrack: Long, idVehicle: Long): Int {
//        return laptimeDao.findAll().filter { it.idTrack == idTrack && it.idVehicles == idVehicle }.count()
        return laptimeDao.geCountForCombo(idTrack, idVehicle)
    }

    fun countTracksIntoLaptimes(): Int {
        return laptimeDao.countTracksIntoLaptimes()
    }
}