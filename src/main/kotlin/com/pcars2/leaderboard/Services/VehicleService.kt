package com.pcars2.leaderboard.Services

import com.pcars2.leaderboard.DAO.VehiclesDao
import com.pcars2.leaderboard.Entities.Vehicles
import org.springframework.stereotype.Service
import java.util.*

@Service
class VehicleService (
        private val vehiclesDao: VehiclesDao
) {
    fun save(vehicle: Vehicles) {
        vehiclesDao.save(vehicle)
    }

    fun getAllVehicles(): List<Vehicles> {
        return vehiclesDao.findAll().toList()
    }

    fun getById(id: Long?): List<Vehicles> {
        return vehiclesDao.findAll().filter { it.id == id }
    }

    fun getByName(name: String?): List<Vehicles> {
        return vehiclesDao.findAllByName(name)
    }

    fun getByCategory(name: String): List<Vehicles> {
        return vehiclesDao.findAll().filter { it.category == name }
    }
}