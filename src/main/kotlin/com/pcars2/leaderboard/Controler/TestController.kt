package com.pcars2.leaderboard.Controler

import com.pcars2.leaderboard.DAO.LaptimeDao
import com.pcars2.leaderboard.DAO.UserDao
import com.pcars2.leaderboard.Services.LaptimeService
import com.pcars2.leaderboard.Services.TrackService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import kotlin.system.measureTimeMillis

@RestController
class TestController (
        private val trackService: TrackService,
        private val laptimeService: LaptimeService,
        private val laptimeDao: LaptimeDao,
        private val userDao: UserDao
)
{
    @GetMapping("/time")
    fun getTime(): ResponseEntity<String> {
        var count = measureTimeMillis {
            laptimeService.getCountForCombo(31280808, 4275744320)
        }
        var countFindAll = measureTimeMillis {
            userDao.findAll().filter { it -> it.id == 76561197960278366 }
        }
        return ResponseEntity.ok("My custom method takes ${count} ms, where the findAll one takes ${countFindAll} ms.")
    }
}