package com.pcars2.leaderboard.Controler

import com.pcars2.leaderboard.Entities.Laptime
import com.pcars2.leaderboard.Services.LaptimeService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class LaptimesController (
        private val laptimeService: LaptimeService
) {
//    @GetMapping("/laptimes")
//    fun getLaptimes(@RequestParam customQuery: Map<String, String>): List<Laptime> {
//        var idUser: Int = -1
//        var nameUser: String = ""
//        var idTrack: Int? = -1
//        var nameTrack: String = ""
//        var idVehicle: Int = -1
//        var nameVehicle: String =""
//
//        if(customQuery.containsKey("username")) {
//            nameUser = customQuery.get("username").toString()
//        }
//        else if (customQuery.containsKey("userid")) {
//            idUser = customQuery.get("userid")!!.toInt()
//        }
//        else if(customQuery.containsKey("trackname")) {
//            nameTrack = customQuery.get("trackname").toString()
//        }
//        else if (customQuery.containsKey("trackid")) {
//            idTrack = customQuery.get("trackid")!!.toInt()
//        }
//        else if(customQuery.containsKey("vehiclename")) {
//            nameVehicle = customQuery.get("vehiclename").toString()
//        }
//        else if (customQuery.containsKey("vehicleid")) {
//            idVehicle = customQuery.get("vehicleid")!!.toInt()
//        }
//
//        return laptimeService.getAllCustom(idUser, nameUser) //TODO to be continued
//    }

    @GetMapping("/Laptimes")
    fun getLaptimes(): List<Laptime> {
        return laptimeService.getAllLaptimes()
    }

    @PostMapping("/Laptimes")
    fun createLaptime(@RequestBody Laptime: Laptime): ResponseEntity<String> {
        laptimeService.save(Laptime)

        return ResponseEntity.ok("Laptime saved")
    }

    @GetMapping("/laptimes/countTracksDone")
    fun countTracksIntoLaptimes(): ResponseEntity<String> {
        val count = laptimeService.countTracksIntoLaptimes()

        return ResponseEntity.ok("Actually, there are ${count} tracks that have been performed.")
    }
}