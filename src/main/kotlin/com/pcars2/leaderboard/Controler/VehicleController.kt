package com.pcars2.leaderboard.Controler

import com.pcars2.leaderboard.Entities.Vehicles
import com.pcars2.leaderboard.Services.VehicleService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class VehiclesController (
        private val vehiclesService: VehicleService
) {
    @GetMapping("/vehicles")
    fun getVehicles(@RequestParam customQuery: Map<String, String>): List<Vehicles> {

        if(customQuery.containsKey("name")) {
            return vehiclesService.getByName(customQuery.get("name"))
        }
        else if (customQuery.containsKey("id")) {
            return vehiclesService.getById(customQuery.get("id")?.toLong())
        }
        else if (customQuery.containsKey("category")) {
            return vehiclesService.getByCategory(customQuery.get("category").toString())
        }
        else {
            return vehiclesService.getAllVehicles()
        }
    }

    @PostMapping("/vehicle")
    fun createVehicle(@RequestBody vehicle: Vehicles): ResponseEntity<String> {
        vehiclesService.save(vehicle)

        return ResponseEntity.ok("Vehicle saved")
    }

    @PostMapping("/vehicles")
    fun createVehicle(@RequestBody vehicles: List<Vehicles>): ResponseEntity<String> {
        vehicles.forEach {
            vehiclesService.save(it)
        }

        return ResponseEntity.ok("Vehicle saved")
    }
}