package com.pcars2.leaderboard.Controler

import com.pcars2.leaderboard.DTO.ComboCardDTO
import com.pcars2.leaderboard.Services.ComboCardService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ComboCardController (
        @Autowired
    private val comboCarService: ComboCardService
) {

    @GetMapping("/combocard")
    fun getCombos(@RequestParam customQuery: Map<String, String>): List<ComboCardDTO> {
        var result : List<ComboCardDTO> = listOf<ComboCardDTO>()

        val userId = customQuery.get("user");
        val trackId = customQuery.get("track");

        if(userId != "" && userId != "undefined") {
            if(trackId != "" && trackId != "undefined") {
                result = comboCarService.foundByComboTrackUser(trackId?.toLong()
                        , userId?.toLong())
            } else {
                result = comboCarService.foundByUser(userId?.toLong())
            }
        }

        return result
    }

}