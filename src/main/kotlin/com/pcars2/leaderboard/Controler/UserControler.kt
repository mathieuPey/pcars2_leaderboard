package com.pcars2.leaderboard.Controler

import com.pcars2.leaderboard.DTO.UserDTO
import com.pcars2.leaderboard.Entities.User
import com.pcars2.leaderboard.Services.UserService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class UserControler(
        private val userService: UserService
) {
    @GetMapping("/users")
    fun getUsers(@RequestParam customQuery: Map<String, String>): List<User> {
        var result : List<User>

        if (customQuery.containsKey("username")) {
            result =  userService.foundAllByName(customQuery.get("username"))
        }
        else {
            result =  userService.getAllUser()
        }

        return result
    }

    @GetMapping("/usersDTO")
    fun getUsersDTO(@RequestParam customQuery: Map<String, String>): List<UserDTO> {
        return userService.getAllUserDTO()
    }

    @GetMapping("/user")
    fun getUser(@RequestParam customQuery: Map<String, String>): List<User> {
        var result : List<User> = listOf<User>()

        if(customQuery.containsKey("username")) {
            result =  userService.getByName(customQuery.get("username"))
        }
        else if (customQuery.containsKey("id")) {
            result =  userService.getById(customQuery.get("id")?.toLong())
        }

        return result
    }

    @PostMapping("/user")
    fun saveUser(@RequestBody user: User): ResponseEntity<String> {
        userService.save(user)

        return ResponseEntity.ok().body("User saved")
    }

    @PostMapping("/users")
    fun saveUser(@RequestBody users: List<User>): ResponseEntity<String> {
        users.forEach {
            userService.save(it)
        }

        return ResponseEntity.ok().body("User saved")
    }
}