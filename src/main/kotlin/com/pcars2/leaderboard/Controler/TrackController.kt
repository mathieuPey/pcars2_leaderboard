package com.pcars2.leaderboard.Controler

import com.pcars2.leaderboard.DTO.TrackDTO
import com.pcars2.leaderboard.DTO.UserDTO
import com.pcars2.leaderboard.Entities.Track
import com.pcars2.leaderboard.Services.TrackService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class TrackController (
        private val trackService: TrackService
) {
    @GetMapping("/track")
    fun getTrack(@RequestParam customQuery: Map<String, String>): List<Track> {

        if (customQuery.containsKey("trackname")) {
            return trackService.getByName(customQuery.get("trackname"))
        }
        else if (customQuery.containsKey("id")) {
            return trackService.getById(customQuery.get("id")?.toLong())
        } else {
            return trackService.getAllTracks()
        }
    }

    @GetMapping("/tracks")
    fun getTracks(@RequestParam customQuery: Map<String, String>): List<Track> {

        if (customQuery.containsKey("user")) {
            return trackService.getAllTracksByUserId(customQuery.get("user")?.toLong()).sortedBy{ it.name }
        } else {
            return trackService.getAllTracks().sortedBy { it.name }
        }
    }

    @GetMapping("/tracksDTO")
    fun getUsersDTO(@RequestParam customQuery: Map<String, String>): List<TrackDTO> {
        if (customQuery.containsKey("user")) {
            return trackService.getAllTracksDTOByUserId(customQuery.get("user")?.toLong()).sortedBy{ it.name }
        } else {
            return trackService.getAllTracksDTO().sortedBy { it.name }
        }
    }

    @PostMapping("/track")
    fun createTrack(@RequestBody track: Track): ResponseEntity<String> {
        trackService.save(track)

        return ResponseEntity.ok("Track saved")
    }

    @PostMapping("/tracks")
    fun createTrack(@RequestBody tracks: List<Track>): ResponseEntity<String> {
        tracks.forEach {
            trackService.save(it)
        }

        return ResponseEntity.ok("Track saved")
    }
}