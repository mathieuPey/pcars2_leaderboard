package com.pcars2.leaderboard.Controler

import com.pcars2.leaderboard.Services.LaptimeService
import com.pcars2.leaderboard.Services.ScrapService
import com.pcars2.leaderboard.Services.TrackService
import com.pcars2.leaderboard.Services.VehicleService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ScrapController(
        private val vehicleService: VehicleService,
        private val trackService: TrackService,
        private val laptimeService: LaptimeService,
        private val scrapService: ScrapService
) {
    @GetMapping("updateVehicles")
    fun updateVehicles(): ResponseEntity<String> {
        val cars = scrapService.scrapVehicles()
        cars.forEach { vehicleService.save(it) }

        return ResponseEntity.ok("${cars.size} cars saved or updated")
    }

    @GetMapping("updateTracks")
    fun updateTracks(): ResponseEntity<String> {
        val tracks = scrapService.scrapTracks()
        tracks.forEach { trackService.save(it) }

        return ResponseEntity.ok("${tracks.size} tracks saved or updated")
    }

    @GetMapping("updateLaptimes")
    fun updateLaptimes(): ResponseEntity<String> {
        var trackListId = trackService.getAllTracks()//.map { it.id }
        var vehicleListId = vehicleService.getAllVehicles()//.map { it.id }
        var laptimeGlobalCount = 0
        var message = ""
        var errorMessage = ""

        trackListId.forEach { track ->
            val trackname = trackService.getById(track.id).first().name
            message += "\n  -${trackname} :"
            vehicleListId.forEach {vehicle ->
                println("Fetching ${track.name} -- ${vehicle.name}\n")
                var laptimeCount = 0
                val vehicleName = vehicleService.getById(vehicle.id).first().name

                try {
                    val laptimes = scrapService.scrapLaptimes(track, vehicle)
                    laptimes.forEach { laptimeService.save(it)
                            .also { laptimeCount++ }
                            .also { laptimeGlobalCount++ } }
                } catch (e: Exception) {
                    errorMessage += "- Error on track ${track.name}, vehicle ${vehicle.name}\n"
                    println("=== ERROR on track ${track.name}, vehicle ${vehicle.name} : ${e.message}")
                }

                message += "\n      - ${vehicleName} : ${laptimeCount} laptimes here"
            }
        }

        return ResponseEntity.ok("${laptimeGlobalCount} laptimes saved or updated \n ${message}")
    }

    @GetMapping("updateLaptimesLight")
    fun updateLaptimesLight(): ResponseEntity<String> {
        var trackListId = trackService.getAllTracks()//.map { it.id }
        var vehicleListId = vehicleService.getAllVehicles()//.map { it.id }
        var laptimeGlobalCount = 0
        var message = ""
        var errorMessage = ""

        trackListId.forEach { track ->
            val trackname = trackService.getById(track.id).first().name
            message += "\n  -${trackname} :"
            vehicleListId.forEach {vehicle ->
                if(laptimeService.getCountForCombo(track.id, vehicle.id) == 0) {
                    println("Fetching ${track.name} -- ${vehicle.name}\n")
                    var laptimeCount = 0
                    val vehicleName = vehicleService.getById(vehicle.id).first().name

                    try {
                        val laptimes = scrapService.scrapLaptimes(track, vehicle)
                        laptimes.forEach { laptimeService.save(it)
                                .also { laptimeCount++ }
                                .also { laptimeGlobalCount++ } }
                    } catch (e: Exception) {
                        errorMessage += "- Error on track ${track.name}, vehicle ${vehicle.name}\n"
                        println("=== ERROR on track ${track.name}, vehicle ${vehicle.name} : ${e.message}")
                    }

                    message += "\n      - ${vehicleName} : ${laptimeCount} laptimes here"
                } else {
                    message += "\n combo track ${track.name}, vehicle ${vehicle.name} already in database"
                }
            }
        }

        return ResponseEntity.ok("${laptimeGlobalCount} laptimes saved or updated \n ${message}")
    }

    @GetMapping("UpdateLaptimesForCombo")
    fun updateLaptimesForCombo(@RequestParam customQuery: Map<String, String>): ResponseEntity<String> {
        if(customQuery.containsKey("idTrack") && customQuery.containsKey("idVehicle")) {
            val track = trackService.getById(customQuery.getValue("idTrack").toLong()).first()
            val vehicle = vehicleService.getById(customQuery.getValue("idVehicle").toLong()).first()
            var laptimeGlobalCount = 0

            try {
                val laptimes = scrapService.scrapLaptimes(track, vehicle)
                laptimes.forEach { laptimeService.save(it)
                        .also { laptimeGlobalCount++ } }

            } catch (e: Exception) {
                println("=== ERROR on track ${track.name}, vehicle ${vehicle.name} : ${e.message}")
            }

            return ResponseEntity.ok("Combo update done with ${laptimeGlobalCount} entries")
        } else {
            return ResponseEntity.badRequest().body("Nope, bad params")
        }
    }
}