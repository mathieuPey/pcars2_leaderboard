package com.pcars2.leaderboard

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Pcars2ScraperApplication

fun main(args: Array<String>) {
	runApplication<Pcars2ScraperApplication>(*args)
}
