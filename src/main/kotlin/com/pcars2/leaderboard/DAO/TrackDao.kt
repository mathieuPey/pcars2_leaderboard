package com.pcars2.leaderboard.DAO

import com.pcars2.leaderboard.DTO.TrackDTO
import com.pcars2.leaderboard.DTO.UserDTO
import com.pcars2.leaderboard.Entities.Track
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface TrackDao : CrudRepository<Track, Int> {

    @Query("SELECT * FROM leaderboard.track WHERE id IN (SELECT id_track FROM leaderboard.laptime WHERE id_user = :idUser)", nativeQuery = true)
    fun foundByUser(@Param("idUser") idUser: Long?): List<Track>

    @Query("SELECT * FROM leaderboard.track WHERE id IN (SELECT id_track FROM leaderboard.laptime WHERE id_user = :idUser)", nativeQuery = true)
    fun foundDTOByUser(@Param("idUser") idUser: Long?): List<TrackDTO>

    @Query("SELECT * FROM leaderboard.track", nativeQuery = true)
    fun findAllDTO(): List<TrackDTO>
}