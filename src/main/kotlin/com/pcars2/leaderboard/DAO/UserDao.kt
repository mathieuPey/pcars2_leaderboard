package com.pcars2.leaderboard.DAO

import com.pcars2.leaderboard.DTO.UserDTO
import com.pcars2.leaderboard.Entities.User
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserDao : CrudRepository<User, Int> {
   fun findByName(username: String?): List<User> {
       return this.findAll().filter { it.name == username }
   }

    @Query("SELECT * FROM leaderboard.user WHERE name LIKE %:name%", nativeQuery = true)
    fun foundAllByName(@Param("name") partialName: String?): List<User>

    @Query("SELECT name FROM leaderboard.user WHERE id = :id", nativeQuery = true)
    fun getNameById(@Param("id") id: Long?): String

    @Query("SELECT * FROM leaderboard.user", nativeQuery = true)
    fun findAllDTO(): List<UserDTO>
}