package com.pcars2.leaderboard.DAO

import com.pcars2.leaderboard.Entities.Laptime
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface ComboCardDao: CrudRepository<Laptime, Int> {

    @Query("SELECT * FROM\n" +
            "(SELECT \n" +
            "\tL.chrono AS chrono,\n" +
            "    L.date AS date,\n" +
            "    U.name AS userName,\n" +
            "    V.name AS vehicleName,\n" +
            "    T.name AS trackName\n" +
            "FROM leaderboard.laptime L \n" +
            "\tINNER JOIN leaderboard.user U ON U.id = L.id_user\n" +
            "    INNER JOIN leaderboard.vehicles V ON V.id = L.id_vehicles\n" +
            "    INNER JOIN leaderboard.track T ON T.id = L.id_track\n" +
            "WHERE L.id_track = :trackId\n" +
            "    AND L.id_vehicles = :vehicleId\n" +
            "ORDER BY chrono ASC) AS Req", nativeQuery = true)
    fun foundByComboTrackVehicle(@Param("trackId") trackId: Long?, @Param("vehicleId") vehicleId: Long?): List<ComboCardSQL>


    // Avec mon user, je vais chercher les vehicles qu'il a sur cette track, et pour chacun de ces combo, je vais
    // chercher les ComboCardSQL
    @Query("SELECT * FROM\n" +
            "(SELECT \n" +
            "\tL.chrono AS chrono,\n" +
            "    L.date AS date,\n" +
            "    U.name AS userName,\n" +
            "    V.name AS vehicleName,\n" +
            "    T.name AS trackName\n" +
            "FROM leaderboard.laptime L \n" +
            "\tINNER JOIN leaderboard.user U ON U.id = L.id_user\n" +
            "    INNER JOIN leaderboard.vehicles V ON V.id = L.id_vehicles\n" +
            "    INNER JOIN leaderboard.track T ON T.id = L.id_track\n" +
            "WHERE L.id_track = :trackId\n" +
            "    AND L.id_vehicles IN (SELECT LAP.id_vehicles FROM leaderboard.laptime LAP WHERE id_user = :userId AND id_track = :trackId)\n" +
            "ORDER BY chrono ASC) AS Req", nativeQuery = true)
    fun foundByComboTrackUser(@Param("trackId") trackId: Long?, @Param("userId") userId: Long?): List<ComboCardSQL>

    interface ComboCardSQL {
            val chrono: Int
            val date: String
            val username: String
            val vehicleName: String
            val trackName: String
    }
}