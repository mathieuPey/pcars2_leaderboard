package com.pcars2.leaderboard.DAO

import com.pcars2.leaderboard.Entities.Laptime
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface LaptimeDao : CrudRepository<Laptime, Int> {

    fun findByIdUser(id: Long): List<Laptime> {
        return this.findAll().filter { it.idUser == id }
    }

    fun findByIdTrack(id: Long): List<Laptime> {
        return this.findAll().filter { it.idTrack == id }
    }

    fun findByIdTrackAndIdVehicles(idTrack: Long, idVehicle: Long): List<Laptime> {
        return this.findAll().filter { it.idTrack == idTrack && it.idVehicles == idVehicle }
    }

    @Query("SELECT COUNT(DISTINCT(id_track)) FROM leaderboard.laptime", nativeQuery = true)
    fun countTracksIntoLaptimes(): Int

    @Query("SELECT COUNT(*) FROM leaderboard.laptime WHERE id_track = ?1 AND id_vehicles = ?2", nativeQuery = true)
    fun geCountForCombo(idTrack: Long, idVehicle: Long): Int
}