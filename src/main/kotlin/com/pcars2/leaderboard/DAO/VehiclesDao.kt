package com.pcars2.leaderboard.DAO

import com.pcars2.leaderboard.Entities.Vehicles
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface VehiclesDao : CrudRepository<Vehicles, Int> {

    fun findAllByName(name: String?): List<Vehicles> {
        return this.findAll().filter { it.name.contains(name.toString()) }
    }
}