package com.pcars2.leaderboard

import com.pcars2.leaderboard.DTO.ComboCardSQLDTO
import com.pcars2.leaderboard.DTO.Tier


class Utils {
    companion object{
        fun convertStringToSeconds(dateString: String): Int {

            val minutes = dateString.split(':')[0].toInt()
            val seconds = dateString.split(':')[1].split('.')[0].toInt()
            val milliseconds = dateString.split(':')[1].split('.')[1].toInt()

            val time = (minutes * 60000) + (seconds * 1000) + (milliseconds)

            return time
        }

        fun convertMillisToFormattedString(ms: Int): String {
            val minutes = ms / 1000 / 60
            val seconds = ms / 1000 % 60
            val milliseconds = ms - (minutes * 60000 + seconds * 1000)

            return minutes.toString().padStart(2, '0') +
                    ":" + seconds.toString().padStart(2, '0') +
                    "." + milliseconds.toString().padStart(3,'0')
        }

        fun composeTiers(listCombo: List<ComboCardSQLDTO>): List<Tier> {
            var result: MutableList<Tier> = mutableListOf()

            val nbParticipants = listCombo.count()
            var floorT0: Int = 1
            var ceilT0 = Math.ceil(nbParticipants.toDouble() * 0.05).toInt()
            var floorT1: Int = ceilT0 + 1
            var ceilT1 = when (Math.ceil(nbParticipants.toDouble() * 0.125).toInt() <= floorT1) {
                true -> floorT1 + 1
                false -> Math.ceil(nbParticipants.toDouble() * 0.125).toInt()
            }
            var floorT2: Int = ceilT1 + 1
            var ceilT2 = when (Math.ceil(nbParticipants.toDouble() * 0.33).toInt() <= floorT2) {
                true -> floorT2 + 1
                false -> Math.ceil(nbParticipants.toDouble() * 0.33).toInt()
            }
            var floorT3: Int = ceilT2 + 1
            var ceilT3: Int = nbParticipants

            var t0 = composeTier(floorT0, ceilT0, nbParticipants, 0, listCombo)
            var t1 = composeTier(floorT1, ceilT1, nbParticipants, 1, listCombo)
            var t2 = composeTier(floorT2, ceilT2, nbParticipants, 2, listCombo)
            var t3 = composeTier(floorT3, ceilT3, nbParticipants, 3, listCombo)

            result.add(t0)
            result.add(t1)
            result.add(t2)
            result.add(t3)

            return result
        }

        fun composeTier(floor: Int, ceil: Int, nbParticipants: Int, level: Int, list: List<ComboCardSQLDTO>): Tier {
            var tier: Tier = Tier(level = -1)
            var orderedListByTime = list.sortedBy { it.chrono }

            // Pour exclure un Tier (level -1), il faut que son floor soit supérieur au nbParticipant
            if(floor < nbParticipants) {
                tier.level = level
                tier.nbUser = ceil - floor
                tier.minTime = convertMillisToFormattedString(orderedListByTime.elementAt(floor - 1).chrono)
                tier.maxTime = convertMillisToFormattedString(orderedListByTime.elementAt(ceil - 1).chrono)
            }

            return tier
        }

        fun computeMyTier(tiers: List<Tier>, myPR: Int): String {
            var result = "tier "

            tiers.sortedBy { it.level }.forEach { currentTier ->
                if (myPR <= convertStringToSeconds(currentTier.maxTime)
                        && myPR > convertStringToSeconds(currentTier.minTime)) {
                    result = result.plus(currentTier.level)
                }
            }

            return result
        }

        fun computeGapToNextTier(tiers: List<Tier>, myPR: Int): Int {
            var result = 0

            tiers.sortedBy { it.level }.forEach { currentTier ->
                if (myPR <= convertStringToSeconds(currentTier.maxTime)
                        && myPR > convertStringToSeconds(currentTier.minTime)) {

                    if (currentTier.level == 0) {
                        result = myPR - convertStringToSeconds(currentTier.minTime)
                    } else {
                        val targetTime = tiers.filter { it.level == currentTier.level-1 }.first().maxTime
                        result = myPR - convertStringToSeconds(targetTime)
                    }

                }
            }

            return result
        }
    }
}