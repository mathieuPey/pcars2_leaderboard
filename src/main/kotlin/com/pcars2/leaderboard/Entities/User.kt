package com.pcars2.leaderboard.Entities

import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class User (
   @Id
   val id: Long = -1,
   val name: String = "default"
)
