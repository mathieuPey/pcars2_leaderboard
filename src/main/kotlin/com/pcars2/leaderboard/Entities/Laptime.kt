package com.pcars2.leaderboard.Entities

import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@IdClass(Laptime::class)
data class Laptime
(
    @Id
    var idTrack: Long = 1L,
    @Id
    var idUser: Long = 1L,
    @Id
    var idVehicles: Long = 1L,
    var chrono: Int = 0,
    var date: LocalDateTime = LocalDateTime.MIN
) : Serializable