package com.pcars2.leaderboard.Entities

import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Vehicles (
    @Id
    val id: Long = -1,
    val name: String = "default",
    val category: String = "default"
)