package com.pcars2.leaderboard

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.util.Assert

internal class UtilsTest {

    @Test
    fun sould_convert_ms_to_formattedString() {
        var mapValues= mutableMapOf<Int, String>()
        mapValues[3] = "00:00.003";
        mapValues[3012] = "00:03.012";
        mapValues[180000] = "03:00.000";

        mapValues.forEach {
          Assertions.assertEquals(Utils.convertMillisToFormattedString(it.key), it.value)
        }
    }



}