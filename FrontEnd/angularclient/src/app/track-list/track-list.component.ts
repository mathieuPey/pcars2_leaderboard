import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Track } from '../../dto/track';
import { TrackService } from '../../services/track.service';

@Component({
  selector: 'app-track-list',
  templateUrl: './track-list.component.html',
  styleUrls: ['./track-list.component.css']
})
export class TrackListComponent implements OnInit, OnChanges {

  @Input() userIdChoosed: string;
  @Output() trackIdChoosed = new EventEmitter<string>();
  tracks: Track[];
  selectedTrack: Track;
  firstLoad = true;

  constructor(private trackService: TrackService) { }

  ngOnInit(): void {
    this.trackService.findAll().subscribe(data => {
      this.tracks = data;
    });

    this.firstLoad = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.firstLoad) {
      if (this.userIdChoosed !== '') {
        this.trackService.findByUser(this.userIdChoosed).subscribe(data => {
          this.tracks = data;

          if (this.selectedTrack !== undefined && this.selectedTrack !== null) {
            if (this.tracks.map(track => track.id).indexOf(this.selectedTrack.id) === -1) {
              this.selectedTrack = null;
              this.emitTrack();
            }
          }

        });
      } else {
          this.trackService.findAll().subscribe(data => {
            this.tracks = data;
          });
      }
    }
  }

  emitTrack() {
      if (this.selectedTrack !== null) {
        this.trackIdChoosed.emit(this.selectedTrack.id);
      } else {
        this.trackIdChoosed.emit('');
      }
  }

}
