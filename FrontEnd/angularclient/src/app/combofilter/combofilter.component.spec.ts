import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CombofilterComponent } from './combofilter.component';

describe('CombofilterComponent', () => {
  let component: CombofilterComponent;
  let fixture: ComponentFixture<CombofilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CombofilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CombofilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
