import {Component, OnInit, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {UpdateCardsDTO} from '../../dto/update-cards-dto';

@Component({
  selector: 'app-combofilter',
  templateUrl: './combofilter.component.html',
  styleUrls: ['./combofilter.component.css']
})
export class CombofilterComponent implements OnInit, OnChanges {

  @Output() updateCardsOutput = new EventEmitter<UpdateCardsDTO>();

  constructor() { }
  updateCardsDTO: UpdateCardsDTO;

  ngOnInit(): void {
    this.updateCardsDTO = new UpdateCardsDTO();
  }

  ngOnChanges(): void {
    this.updateCardsOutput.emit(this.updateCardsDTO);
  }

  defineUser(userId: string) {
    this.updateCardsDTO.userId = userId;
    this.ngOnChanges();
  }

  defineTrack(trackId: string) {
    this.updateCardsDTO.trackId = trackId;
    this.ngOnChanges();
  }


}
