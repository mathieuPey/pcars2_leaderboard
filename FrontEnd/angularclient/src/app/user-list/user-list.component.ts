import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { User } from '../../dto/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  @Output() userIdChoosed = new EventEmitter<string>();

  users: User[];
  selectedUser: User;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.findAll().subscribe(data => {
      this.users = data;
    });
  }

  emitUser() {
    if (this.selectedUser !== null) {
      this.userIdChoosed.emit(this.selectedUser.id);
    } else {
      this.userIdChoosed.emit('');
    }
  }

}
