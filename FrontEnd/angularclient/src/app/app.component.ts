import { Component } from '@angular/core';
import { Combocarddto } from '../dto/combocarddto';
import { CombocardService } from '../services/combocard.service';
import { UpdateCardsDTO } from '../dto/update-cards-dto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Project cars2 Leaderboard';
  comboIds = '';
  trackId = '';
  userId = '';
  categoryId = '';
  updateCardsDTO: UpdateCardsDTO;
  combocards: Combocarddto[];

  constructor(private combocardService: CombocardService) {
    this.updateCardsDTO = new UpdateCardsDTO();
  }

  updateCards(updateCardDTO: UpdateCardsDTO) {
    this.updateCardsDTO = updateCardDTO;
    this.combocardService.findAllWithParams(updateCardDTO).subscribe(data => {
      this.combocards = data;
    });
  }

}
