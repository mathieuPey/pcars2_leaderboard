import {Component, Input, OnInit} from '@angular/core';
import {Combocarddto} from '../../dto/combocarddto';

@Component({
  selector: 'app-combocard',
  templateUrl: './combocard.component.html',
  styleUrls: ['./combocard.component.css']
})
export class CombocardComponent implements OnInit {

  @Input() card: Combocarddto;
  track: string;
  vehicule: string;
  pr: string;
  myTier: string;
  gap: string;
  bestOfAll: string;

  constructor() { }

  ngOnInit(): void {
    this.track = this.card.trackName;
    this.vehicule = this.card.vehicleName;
    this.pr = this.card.personalBestTime;
    this.myTier = this.card.personalTier;
    this.gap = this.card.gapToNextTier;
    this.bestOfAll = this.card.bestOfAllTime;
  }

}
