import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CombocardComponent } from './combocard.component';

describe('CombocardComponent', () => {
  let component: CombocardComponent;
  let fixture: ComponentFixture<CombocardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CombocardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CombocardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
