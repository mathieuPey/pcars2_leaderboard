import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Track } from '../dto/track';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TrackService {
  private tracksUrl: string;
  private tracksDTOUrl: string;
  private tracksUrlByUSer: string;

  constructor(private http: HttpClient) {
    this.tracksUrl = '//192.168.1.20:9000/tracks';
    this.tracksDTOUrl = '//192.168.1.20:9000/tracksDTO';
  }

  public findAll(): Observable<Track[]> {
    return this.http.get<Track[]>(this.tracksDTOUrl);
  }

  public findByUser(idUser: string): Observable<Track[]> {
    let params = new HttpParams();
    params = params.append('user', idUser);

    return this.http.get<Track[]>(this.tracksDTOUrl, { params });
  }
}
