import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../dto/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private usersURL: string;

  constructor(private http: HttpClient) {
    this.usersURL = '//192.168.1.20:9000/';
  }

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.usersURL + 'usersDTO');
  }
}
