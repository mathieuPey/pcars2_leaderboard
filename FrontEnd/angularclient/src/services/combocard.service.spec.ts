import { TestBed } from '@angular/core/testing';

import { CombocardService } from './combocard.service';

describe('CombocardService', () => {
  let service: CombocardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CombocardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
