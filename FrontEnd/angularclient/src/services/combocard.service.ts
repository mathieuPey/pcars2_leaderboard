import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Combocarddto} from '../dto/combocarddto';
import {UpdateCardsDTO} from '../dto/update-cards-dto';

@Injectable({
  providedIn: 'root'
})
export class CombocardService {

  private combocardURL: string;

  constructor(private http: HttpClient) {
    this.combocardURL = '//192.168.1.20:9000/combocard';
  }

  public findAllByUser(idUser: string): Observable<Combocarddto[]> {
    let params = new HttpParams();
    params = params.append('user', idUser);

    return this.http.get<Combocarddto[]>(this.combocardURL, {params});
  }

  public findAllWithParams(updatecardDTO: UpdateCardsDTO): Observable<Combocarddto[]> {
    let params = new HttpParams();
    params = params.append('user', updatecardDTO.userId);
    params = params.append('track', updatecardDTO.trackId);
//    params = params.append('categoryId', updatecardDTO.categoryId);

    return this.http.get<Combocarddto[]>(this.combocardURL, {params});
  }

}
