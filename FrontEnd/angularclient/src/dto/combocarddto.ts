export class Combocarddto {
  trackName: string;
  vehicleName: string;
  bestOfAllTime: string;
  personalBestTime: string;
  nbLaptime: number;
  personalRanking: number;
  personalTier: string;
  gapToNextTier: string;
  tiers: Tier[];
}

export class Tier {
  level: number;
  minTime: string;
  maxTime: string;
  nbUser: number;
}
