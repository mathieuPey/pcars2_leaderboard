export class Laptime {
  id_vehicles: number;
  id_user: number;
  id_track: number;
  chrono: number;
  date: string;
}
